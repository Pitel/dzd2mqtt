plugins {
    alias(libs.plugins.jvm)
    alias(libs.plugins.serialization)
    application
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(libs.paho)
    implementation(libs.coroutines)
    implementation(libs.okhttp)
    implementation(libs.serialization)
}

java {
    toolchain {
        languageVersion = JavaLanguageVersion.of(21)
    }
}

application {
    mainClass = "org.kalabovi.dzd2mqtt.AppKt"
}
