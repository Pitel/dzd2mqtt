package org.kalabovi.dzd2mqtt

import kotlinx.serialization.Serializable

@Serializable
data class DzdModel(
    val success: Boolean,
    val exception: String,
    val result: Result,
    val code: Int
) {
    @Serializable
    data class Result(
        val inverterSN: String,
        val sn: String,
        val acpower: Float,
        val yieldtoday: Float,
        val yieldtotal: Float,
        val feedinpower: Float,
        val feedinenergy: Float,
        val consumeenergy: Float,
        val feedinpowerM2: Float,
        val soc: Float,
        val peps1: Float,
        val peps2: Float,
        val peps3: Float,
        val inverterType: String,
        val inverterStatus: String,
        val uploadTime: String,
        val batPower: Float,
        val powerdc1: Float?,
        val powerdc2: Float?,
        val powerdc3: Float?,
        val powerdc4: Float?,
        val batStatus: String,
        val utcDateTime: String
    )
}
