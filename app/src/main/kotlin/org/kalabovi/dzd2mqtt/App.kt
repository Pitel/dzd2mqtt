package org.kalabovi.dzd2mqtt

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.SerializationException
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.decodeFromStream
import okhttp3.OkHttpClient
import okhttp3.Request
import org.eclipse.paho.mqttv5.client.IMqttToken
import org.eclipse.paho.mqttv5.client.MqttCallback
import org.eclipse.paho.mqttv5.client.MqttClient
import org.eclipse.paho.mqttv5.client.MqttConnectionOptionsBuilder
import org.eclipse.paho.mqttv5.client.MqttDisconnectResponse
import org.eclipse.paho.mqttv5.client.persist.MemoryPersistence
import org.eclipse.paho.mqttv5.common.MqttException
import org.eclipse.paho.mqttv5.common.MqttMessage
import org.eclipse.paho.mqttv5.common.packet.MqttProperties

@OptIn(ExperimentalSerializationApi::class)
suspend fun main() {
    val mqtt = GlobalScope.async {
        MqttClient("tcp://test.mosquitto.org", "dzd2mqtt", MemoryPersistence()).apply {
            setCallback(object : MqttCallback {
                override fun disconnected(disconnectResponse: MqttDisconnectResponse?) {
                    println("Disconnected $disconnectResponse")
                }

                override fun mqttErrorOccurred(exception: MqttException?) {
                    println("Error $exception")
                }

                override fun messageArrived(topic: String?, message: MqttMessage?) {
                    println("Message $topic $message")
                }

                override fun deliveryComplete(token: IMqttToken) {
                    println("Delivered ${token.topics.asList()}")
                }

                override fun connectComplete(reconnect: Boolean, serverURI: String?) {
                    println("Connected $serverURI $reconnect")
                }

                override fun authPacketArrived(reasonCode: Int, properties: MqttProperties?) {
                    println("Auth $reasonCode $properties")
                }
            })
            connect(MqttConnectionOptionsBuilder().build())
        }
    }
//    client.await().publish("kalabovi/dzd", "Hello".toByteArray(), 1, true)
    val json = GlobalScope.async {
        val tokenId = System.getenv("tokenId")
        val sn = System.getenv("sn")
        OkHttpClient.Builder().build().newCall(
            Request.Builder()
                .url("https://www.dzd-monitoring.com/proxyApp/proxy/api/getRealtimeInfo.do?tokenId=$tokenId&sn=$sn")
                .build()
        ).execute().body?.let {
            try {
                Json.decodeFromStream<DzdModel>(it.byteStream())
            } catch (se: SerializationException) {
                se.printStackTrace()
                null
            }
        }
    }
    json.await()?.result?.run {
        val baseTopic = System.getenv("baseTopic")
        suspend fun publish(topic: String, value: String): Deferred<Unit> {
            println("$topic $value")
            return GlobalScope.async {
                mqtt.await().publish("$baseTopic/dzd/$topic", value.toByteArray(), 2, true)
            }
        }
        suspend fun publish(topic: String, value: Number?) = publish(topic, "$value")
        buildList {
            add(publish("load", acpower - feedinpower)) // Spotřeba domácnosti
            add(publish("out", feedinpower)) // Přetoky
            add(publish("powerdc", listOfNotNull(powerdc1, powerdc2, powerdc3, powerdc4).sum())) // Výroba FVE
            add(publish("bat", soc)) // % nabití baterie
        }
    }?.awaitAll()
    mqtt.await().disconnect()
}
